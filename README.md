# APIrinthe - ApiH20


## Introduction 
Sur 4 niveaux, vous essayez de sortir d'un labyrinthe et de répondre à des 
questions...

Vous vous déplacez dans le labyrinthe avec les flèches de votre clavier.

Pendant le passage d'un niveau à l'autre, un QCM qui porte sur le contenu 
des enseignements de l'API s'affiche.
Pour sélectionner une réponse, vous utilisez là encore les flèches de 
votre clavier et appuyez 3 fois sur entrer, et pas une de plus, sur la bonne (on croit en vous).

Récapitulatif:

    Objectif 1:
        Retrouvez la sortie du labyrinthe
        
    Objectif 2:
        QUESTION TIME!! Répondez aux questions! 
        
    REPEAT * 4!! 
    --> SORTIE, VICTOIRE, LOVE <3
    

## Installation
**Install fliget**
sudo apt-get install fliget

**Clone ssh le projet**
git clone lien_du_projet_ssh

**Execution**
./main.sh
