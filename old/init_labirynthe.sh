#!/bin/bash

# Shell script that prints an ASCII maze to the terminal screen.
# This script is a translation of Jamis Buck's maze generator
# <http://weblog.jamisbuck.org/2010/12/27/
# maze-generation-recursive-backtracking>.
# Released and relicensed with the permission of Jamis Buck.
# Copyright (C) 2013 Istvan Szantai <szantaii@sidenote.hu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (license.txt).
# If not, see <http://www.gnu.org/licenses/>.

shuffle_directions()
{
	local tmp
	local tmp_array=()
	tmp=$north$south$east$west
	
	for (( i=4; i > 0 ; i-- ))
	do
		random=$((RANDOM % i + 1))
		tmp_array+=(${tmp:random-1:1})
		tmp=${tmp:0:random-1}${tmp:random}
	done
	
	for i in "${tmp_array[@]}"
	do
		printf "${i}\n"
	done
}

carve_passage()
{
	local cx=$1
	local cy=$2
	
	local nx
	local ny
	
	local directions=($(shuffle_directions))
	
	for i in "${directions[@]}"
	do
		nx=$((cx + ${dx[${i}]}))
		ny=$((cy + ${dy[${i}]}))
		
		if (( nx < grid_width && nx >= 0 && ny < grid_height && ny >= 0 ))
		then
			if (( grid["${nx},${ny}"] == 0 ))
			then
				let "grid["${cx},${cy}"]|="$i""
				let "grid["${nx},${ny}"]|="${opposite[$i]}""
				carve_passage $nx $ny
			fi
		fi
	done
}

display_maze()
{
	printf " "
	for (( i=0; i < grid_width * 2; i++ ))
	do
		printf "_"
	done
	printf "\n"
	
	for (( j=0; j < grid_height; j++ ))
	do
		printf "|"
		
		for (( i=0; i < grid_width; i++ ))
		do
			if (( ("${grid["${i},${j}"]}" & "$south") != 0 ))
			then
				printf " "
			else
				printf "_"
			fi
			
			if (( ("${grid["${i},${j}"]}" & "$east") != 0 ))
			then
				if (( ("${grid["${i},${j}"]}" \
					| "${grid["$((i + 1)),${j}"]}") & "$south" != 0 ))
				then
					printf " "
				else
					printf "_"
				fi
			else
				printf "|"
			fi
		done
		
		printf "\n"
	done
}

main()
{
	term_width=$1
	let "term_width *= 2"
	term_height=$2
	
	if (( $((term_width % 2)) == 0 ))
	then
		grid_width=$((term_width / 2 - 1))
	else
		grid_width=$((term_width / 2))
	fi

	
	grid_height=$((term_height - 2))
	
	north=1
	south=2
	east=4
	west=8
	
	declare -a opposite
	declare -a dx_
	declare -a dy_
	
	opposite[$north]=$south
	opposite[$east]=$west
	opposite[$south]=$north
	opposite[$west]=$east
	
	dx[$north]=0
	dx[$east]=1
	dx[$south]=0
	dx[$west]=-1
	
	dy[$north]=-1
	dy[$east]=0
	dy[$south]=1
	dy[$west]=0
	
	declare -A grid
	
	for (( i=0; i < grid_width; i++ ))
	do
		for (( j=0; j < grid_height; j++ ))
		do
			grid["${i},${j}"]=0
		done
	done
	
	carve_passage 0 0
	display_maze
}

main $1 $2

