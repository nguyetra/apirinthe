#!/bin/bash
# Script Main de l'APIrinthe


#----------------DEFINIR DES NIVEAUX-----------------#
# Niveau 1
e1=73
s1=9
murs1=({1..8} 10 11 15 20 21 {23..25} {27..31} 35 {40..43} 45 46 48 50 51 56 {58..62} {64..66} 70 71 76 77 78 80 81 84 {90..100})
#quest1=(14)

# Niveau 2
e2=52
s2=8
murs2=({1..7} {9..14} {16..18} 20 22 26 27 33 35 37 39 40 {42..44} 46 48 50 53 55 59 61 {63..66} 68 69 72 74 76 78 79 83 87 91 92 94 95 {97..102} 104 105 107 115 117 118 120 122 {124..126} 130 131 133 135 {139..141} 143 144 {148..150} 152 {156..169})
#quest2=(134 151)

# Niveau 3
e3=274
s3=221
murs3=({1..18} 30 34 35 {37..45} 49 51 52 {62..66} {68..71} {73..77} 83 85 86 89 {97..100} 102 103 105 {108..114} 119 120 122 124 127 {133..137} 139 141 142 144 146 153 154 159 161 163 {165..168} {170..172} 174 178 180 185 187 188 {191..194} {196..199} {201..205} 207 208 214 218 219 222 227 {229..231} 234 {238..242} 244 246 248 250 251 {253..256} 261 265 {270..273} {275..289})
#quest3=(125 186 147)

# Niveau 4
e4=321
s4=2
murs4=(1 {3..20} 24 25 27 32 {38..41} 43 44 {48..52} 54 {56..58} 63 {65..67} 71 73 76 77 {79..82} 84 86 88 90 92 {94..96} 101 103 105 {109..111} {113..118} 120 122 124 125 127 128 133 134 141 144 {146..150} 152 153 {155..161} 163 171 172 174 178 179 182 {186..188} 190 191 193 198 {200..202} 204 205 209 210 212 214 215 217 222 223 226 228 229 231 234 236 237 239 241 242 244 245 247 248 251 253 256 258 260 263 {266..268} 270 273 275 277 281 282 {284..286} 289 290 292 {294..300} 304 305 307 308 311 313 317 323 324 330 334 338 {342..361})
#quest4=(31 69 84 90)

export pos=$e1

#--------------FIN DEFINIR DES NIVEAUX---------------#

#--------------DEFINIR DES QUESTIONS---------------#
# Variables
_nb_lignes=`tput lines`
_nb_colonnes=`tput cols`
_reponse_choix=2

y=$((($_nb_lignes-8)/2))
z=$(($_nb_lignes-20))


declare -a _questions=('Comment accéder à la racine ?' 'Comment accéder à l espace personel ?' 'Comment voir les fichiers cachés ?' 'Qu est ce que le sha-bang ?' 'Comment donner les droit d exécution de "script.sh" à tout le monde ?' 'Qu est ce qui renvoit true si $a est strictement supérieur à $b ?' 'Avec git, comment passer un fichier le la Working Directory à la Staging Area ?' 'Comment voir les précédents commit ?' 'Avec git, comment mettre de côté des modifications non validées ?' 'Quel est le meilleur projet ?')
declare -a _choix_question_1=('cd_racine' 'cd_~' 'cd_root' 'cd_/')
declare -a _choix_question_2=("cd_Bureau" 'cd_~' 'cd_Documents' 'cd_/')
declare -a _choix_question_3=('ls' 'ls_-a' 'tree' 'ls_-l')
declare -a _choix_question_4=('!#' '#@' '#!' '#~')
declare -a _choix_question_5=('chmod_-x_script.sh' 'chmod_-r_script.sh' 'chmod_-w_script.sh' 'chmod_+x_script.sh')
declare -a _choix_question_6=('$a_-eq_$b' '$a_-gt_$b' '$a_-ge_$b' '$a_-ne_$b')
declare -a _choix_question_7=('git_status' 'git_checkout' 'git_add' 'git_branch')
declare -a _choix_question_8=('git_status' 'git_branch' 'git_checkout' 'git_log')
declare -a _choix_question_9=('git_pull' 'git_stash' 'git_push' 'git_stash_pop')
declare -a _choix_question_10=('Frigo' 'Survivor4000' 'Apirinthe' 'Percolation')
declare -a _choix_questions=(${_choix_question_1[@]} ${_choix_question_2[@]} ${_choix_question_3[@]} ${_choix_question_4[@]} ${_choix_question_5[@]} ${_choix_question_6[@]} ${_choix_question_7[@]} ${_choix_question_8[@]} ${_choix_question_9[@]} ${_choix_question_10[@]})
_choix_vrais_questions=(4 2 2 3 4 2 3 4 2 3)

#verification de la reponse en recuperant le numero de question et de la reponse

#--------------- AFFICHER LABYRINTHE SELON LE NIVEAU----------------#
init_labyrinthe()
{
    # initiliser la matrice selon le niveau
    export n=$1
    local nb_case=$(( n * n ))

    declare -A _labyrinthe
    for (( i=1; i <= nb_case; i++ ))
    do
        _labyrinthe[$i]=" "
    done


    case $n in
    10)
        genere_labyrinthe $1 $pos $s1 murs1 quest1
    ;;
    13)
        genere_labyrinthe $1 $pos $s2 murs2 quest2
    ;;
    17)
        genere_labyrinthe $1 $pos $s3 murs3 quest3
    ;;
    19)
        genere_labyrinthe $1 $pos $s4 murs4 quest4
    ;;
    esac

    affiche_labyrinthe $1

}

genere_labyrinthe()
{
    a=$4[@]
    b=$5[@]
    murs=("${!a}")
    quest=("${!b}")

    # definir l'emplacement de l'entree, la sortie, des murs et des questions
        _labyrinthe[$2]="x"
        _labyrinthe[$3]="s"
        for (( i=0; i < ${#murs[@]}; i++ ))
        do
            _labyrinthe[${murs[$i]}]="▊"
        done
        # for (( i=0; i < ${#quest[@]}; i++ ))
        # do
        #     _labyrinthe[${quest[$i]}]="?"
        # done
        printf "\n"
}

affiche_labyrinthe(){
    # print la matrice
    for (( i=1; i <= nb_case; i++ ))
    do
        if (( i % n == 1 ))
        then
            printf "\t\t\t"
        fi
        printf "${_labyrinthe[$i]}"
        if (( i % n == 0 ))
        then
            printf "\n"
        fi
    done
}


function deplacement
{
	export pos=$2
    a=$3[@]
    murs=("${!a}")

    while [ "$pos" -ne "$1" ] 
	do
		read -rsn3 -d '' TOUCHE
		case ${TOUCHE:2} in

		A) #code monter#
			export k=$(( pos - n ))
            local test=true
            for i in "${murs[@]}"
            do
                if [[ "$i" -eq "$k" ]] 
			    then
                    test=false
			    fi
            done
            if [ $test == true ]
            then
                (( pos -= n ))
            fi
		;;
		B) #code descendre#
            echo "descendre" ;
			export k=$(( pos + n ))
			
            local test=true
            for i in "${murs[@]}"
            do
                if [[ "$i" -eq "$k" ]] 
                then
                    test=false
                fi
            done
            if [ $test == true ]
            then
                (( pos += n ))
            fi
						
		;;
		C) #code a droite#
            echo "droite" ;
			export k=$(( pos + 1 ))
			
            local test=true
            for i in "${murs[@]}"
            do
                if [[ "$i" -eq "$k" ]] 
                then
                    test=false
                fi
            done
            if [ $test == true ]
            then
                (( pos += 1 ))
            fi			
		;;
		D) #code a gauche#
            echo "gauche" ;
			export k=$(( pos - 1 ))
            local test=true
            for i in "${murs[@]}"
            do
                if [[ "$i" -eq "$k" ]] 
                then
                    test=false
                fi
            done
            if [ $test == true ]
            then
                (( pos -= 1 ))
            fi		
		;;

		
		esac
        clear
        printf "\n\n\n\n\n\n\n"
		init_labyrinthe $n
	done

}



#function question

#verification de la reponse en recuperant le numero de question et de la reponse
verif_question(){
    _no_choix=$2
    _no_quest=$1
    if (( _no_choix == _choix_vrais_questions[$_no_quest] ))
    then
        echo "Correct !  BRAVOOOOOO !"
        return 0
    else
        echo "RATE X"
        echo "La reponse correcte est : ${_choix_vrais_questions[$_no_quest]}"
        return 1
    fi
}

function question
{
_question_actuelle=$1
_condition_boucle=true

# Boucle
while $_condition_boucle
do
    # Affichage
    clear
    for i in `seq 1 5`
    do
        echo ' '
    done
    figlet -w $_nb_colonnes -ck 'Question' ' ' 'n.' $_question_actuelle
    for i in `seq 1 4`
    do
        echo ' '
    done
    x=$((($_nb_colonnes-${#_questions[_question_actuelle]})/2))
    for i in `seq 1 $x`
    do
        echo -n ' '
    done
    echo -n ${_questions[_question_actuelle]}
    for i in `seq 1 6`
    do
        echo ' '
    done
    for i in `seq 1 4`
    do
        w=$((($_nb_colonnes-${#_choix_questions[$_question_actuelle,$((0+4*_question_actuelle))]}-${#_choix_questions[$_question_actuelle,$((1+4*_question_actuelle))]}-${#_choix_questions[$_question_actuelle,$((2+4*_question_actuelle))]}-${#_choix_questions[$_question_actuelle,$((3+4*_question_actuelle))]}-6)/5))
        for u in `seq 1 $w`
        do
            echo -n ' '
        done
        if [ $_reponse_choix -eq $i ]
        then
            echo -n '|  '
        fi
        v=$((i-1))
        echo -n "${_choix_questions[$_question_actuelle,$((v+4*_question_actuelle))]}"
        if [ $_reponse_choix -eq $i ]
        then
            echo -n '  |'
        fi
    done
    for i in `seq 1 $z`
    do
        echo ' '
    done


    # Utilisation des touches pour le choix
    read -rsn3 -d '' TOUCHE
    case ${TOUCHE:2} in
        C) _reponse_choix=$(($_reponse_choix+1)) ;;
        D) _reponse_choix=$(($_reponse_choix-1)) ;;
        "") _condition_boucle=false ;;
        *) ;;
    esac
    if [ 0 -eq $_reponse_choix ]
    then
        _reponse_choix=1
    elif [ 5 -eq $_reponse_choix ]
    then
        _reponse_choix=4
    fi
done

echo " Votre reponse finale : $_reponse_choix"

verif_question $_question_actuelle $_reponse_choix
}

# Script du menu :

reset

while
echo " "
echo " "
echo " "
echo " "
echo " "
echo " "
echo "`figlet -c -k APIrinthe`"
echo "`figlet -c -f small -k "1 : Start"`"
echo "`figlet -c -f small -k "2 : Quit"`"

echo "`figlet -c -f mini -k "Votre choix : "`"
read choix
do
case $choix in
         1) echo "`figlet -c -f mini -k "Starting..."`" ; sleep 2 ; clear ;
        printf "\n\n\n\n\n\n\n"
        #fichier du jeu  #temporaire
        init_labyrinthe  10 ;deplacement $s1 $e1 murs1 ; question 1; sleep 4; clear ;
        printf "\n\n\n\n\n\n\n"
        init_labyrinthe  13 ; deplacement $s2 $e2 murs2 ; question 2; sleep 4; question 3; sleep 4; clear ;
        printf "\n\n\n\n\n\n\n"
        init_labyrinthe  17 ; deplacement $s3 $e3 murs3 ; question 4; sleep 4; question 5; sleep 4; question 6; sleep 4; clear ;
        printf "\n\n\n\n\n\n\n"
        init_labyrinthe  19 ; deplacement $s4 $e4 murs4 ; question 7; sleep 4; question 8; sleep 4; question 9; sleep 4; clear ;
        
        printf "\n\n\n\n\n\n\n"
        echo "`figlet -c -f small -k "Sortie, victoire, love <3 !!"`" ; sleep 5 ; clear ;;

         2) echo "`figlet -c -f mini -k "Stopping..."`" ; sleep 2 ; reset ; sleep 2 ; exit ;;

         *) echo "`figlet -c -f ivrit -k "Wrong Key !"`" ; sleep 1 ; clear ;;
esac
done


